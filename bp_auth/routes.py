from flask import Blueprint, session, render_template, request, current_app

import access
from sql_provider import SQLProvider
from usedatabase import work_with_db

auth_app = Blueprint('auth', __name__, template_folder='templates')

provider = SQLProvider('bp_auth/sql')


@auth_app.route('/login', methods=['GET', 'POST'])
def login_page():
    if request.method == 'GET':
        return render_template('login_form.html')
    else:
        login = request.form.get('login')
        password = request.form.get('password')

        sql = provider.get('user.sql', login=login, password=password)

        result, _ = work_with_db(current_app.config['DB_CONFIG'], sql)

        if result:
            session['group_name'] = result[0]['role']
            return render_template('success.html', context={'role': access.get_role()})
        return render_template('fail.html')
