from flask import Blueprint, render_template, request, current_app, session

import access
from sql_provider import SQLProvider
from usedatabase import work_with_db
from access import group_permission_decorator, group_permission_validation

query_app = Blueprint('query', __name__, template_folder='templates')

provider = SQLProvider('bp_query/sql')


@query_app.route('/')
@group_permission_decorator
def query_menu():
    context = {'group_permission_validation': group_permission_validation, 'role': access.get_role()}
    return render_template('query_menu.html', context=context)


@query_app.route('/interview')
@group_permission_decorator
def interview():
    employee_name = request.args.get('employee_name')
    date = request.args.get('date')
    if employee_name is None or date is None:
        return render_template('interview_form.html')
    else:
        year, month = date.split('-')
        sql = provider.get('interview.sql', name=employee_name, year=year, month=month)
        db_config = current_app.config['DB_CONFIG']
        result, schema = work_with_db(db_config, sql)
        context = {'schema': ['Дата интервью','Название вакансии', 'Имя кандидата', 'Результат', 'HR'], 'data': result}
        return render_template('interview_result.html', context=context)


@query_app.route('/vacancy')
@group_permission_decorator
def vacancy():
    date = request.args.get('date')
    if date is None:
        return render_template('vacancy_form.html')
    year, month = date.split('-')
    sql = provider.get('vacancy.sql', year=year, month=month)
    db_config = current_app.config['DB_CONFIG']
    result, schema = work_with_db(db_config, sql)
    context = {'schema': ['№', 'Название вакансии', 'Дата открытия вакансии', 'Количество дней, когда вакансия была свободна', 'Число кандидатов'], 'data': result}
    return render_template('vacancy_result.html', context=context)


@query_app.route('/young_employee')
@group_permission_decorator
def young_employee():
    year_month = request.args.get('date')
    if year_month is None:
        return render_template('young_employee_form.html')
    year, year_month = year_month.split('-')
    sql = provider.get('young_employee.sql', year=year, month=year_month)
    db_config = current_app.config['DB_CONFIG']
    result, schema = work_with_db(db_config, sql)
    context = {'schema': ['№', 'Имя', 'Дата рождения', 'Образование', 'Номер отдела', 'Дата приема на работу', 'Позиция', 'Оклад', 'Дата увольнения'], 'data': result}
    return render_template('young_employee_result.html', context=context)


@query_app.route('/employee')
@group_permission_decorator
def employee():
    date = request.args.get('date')
    if date is None:
        return render_template('employee_form.html')
    year, date = date.split('-')
    sql = provider.get('employee.sql', year=year, month=date)
    db_config = current_app.config['DB_CONFIG']
    result, _ = work_with_db(db_config, sql)
    context = {'schema': ['Имя'], 'data': result}
    return render_template('employee_result.html', context=context)
