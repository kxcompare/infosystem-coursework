from flask import Blueprint, session, render_template, request, current_app, redirect

from access import group_permission_validation, get_role
from sql_provider import SQLProvider
from usedatabase import work_with_db, update_db
from bp_process.utils import add_to_basket, clear_basket

process_app = Blueprint('process', __name__, template_folder='templates')

provider = SQLProvider('bp_process/sql')


@process_app.route('/')
def process_menu():
    context = {'group_permission_validation': group_permission_validation, 'role': get_role()}
    return render_template('process_menu.html', context=context)


@process_app.route('/vacancy', methods=['GET', 'POST'])
def vacancy():
    if request.method == 'GET':
        result, _ = work_with_db(current_app.config['DB_CONFIG'], provider.get('vacancy.sql'))
        schema = ['Название позиции', 'Дата открытия', 'Дата закрытия']
        context = {'schema': schema, 'data': result, 'role': get_role()}
        return render_template('vacancy_list.html', context=context)
    else:
        vac_id = request.form.get('id')
        update_db(current_app.config['DB_CONFIG'], provider.get('delete_interview_by_vac.sql', id=vac_id))
        update_db(current_app.config['DB_CONFIG'], provider.get('delete_vacancy.sql', id=vac_id))
        return redirect('/process/vacancy')


@process_app.route('/vacancy/add', methods=['GET', 'POST'])
def add_vacancy():
    if request.method == 'GET':
        return render_template('vacancy_add.html')
    else:
        position = request.form.get('position')
        date = request.form.get('date')
        update_db(current_app.config['DB_CONFIG'], provider.get('insert_vacancy.sql', position=position, date=date))
        return redirect('/process/vacancy')


@process_app.route('/candidate', methods=['GET', 'POST'])
def candidate():
    if request.method == 'GET':
        result, _ = work_with_db(current_app.config['DB_CONFIG'], provider.get('candidate.sql'))
        schema = ['Имя', 'Дата рождения', 'Образование']
        context = {'schema': schema, 'data': result, 'role': get_role()}
        return render_template('candidate_list.html', context=context)
    else:
        candidate_id = request.form.get('id')
        update_db(current_app.config['DB_CONFIG'], provider.get('delete_interview_by_candidate.sql', id=candidate_id))
        update_db(current_app.config['DB_CONFIG'], provider.get('delete_candidate.sql', id=candidate_id))
        return redirect('/process/candidate')


@process_app.route('/candidate/add', methods=['GET', 'POST'])
def add_candidate():
    if request.method == 'GET':
        return render_template('candidate_add.html')
    else:
        name = request.form.get('name')
        birthday = request.form.get('birthday')
        education = request.form.get('education')
        update_db(current_app.config['DB_CONFIG'],
                  provider.get('insert_candidate.sql', name=name, birthday=birthday, education=education))
        return redirect('/process/candidate')


@process_app.route('/interview', methods=['GET', 'POST'])
def interview():
    if request.method == 'GET':
        result, _ = work_with_db(current_app.config['DB_CONFIG'], provider.get('interview.sql'))
        schema = ['Дата', 'Название позиции', 'Кандидат', 'Собеседующий', 'Результат собеседования']
        context = {'schema': schema, 'data': result, 'role': get_role()}
        return render_template('interview_list.html', context=context)
    else:
        interview_id = request.form.get('id')
        update_db(current_app.config['DB_CONFIG'], provider.get('delete_interview.sql', id=interview_id))
        return redirect('/process/interview')


@process_app.route('/interview/add', methods=['GET', 'POST'])
def add_interview():
    if request.method == 'GET':
        vac_sql = provider.get('open_vac.sql')
        open_vac, _ = work_with_db(current_app.config['DB_CONFIG'], vac_sql)
        hr, _ = work_with_db(current_app.config['DB_CONFIG'], provider.get('hr.sql'))
        candidates, _ = work_with_db(current_app.config['DB_CONFIG'], provider.get('candidate.sql'))
        basket = session.get('basket', [])
        return render_template('interview_add.html', open_vac=open_vac, hr=hr, candidates=candidates, items=candidates,
                               basket=basket, role=get_role())
    else:
        item_id = request.form.get('c_id')
        items, _ = work_with_db(current_app.config['DB_CONFIG'], provider.get('get_candidate.sql', id=item_id))
        if items:
            add_to_basket(items[0])
        return redirect('/process/interview/add')


@process_app.route('/interview/set', methods=['GET', 'POST'])
def basket_buy():
    basket = session.get('basket', [])
    if request.method == 'POST':
        date = request.form.get('date')
        v_id = request.form.get('v_id')
        e_id = request.form.get('e_id')
        for item in basket:
            sql = provider.get('insert_interview.sql', date=date, e_id=e_id, c_id=item['c_id'], v_id=v_id)
            update_db(current_app.config['DB_CONFIG'], sql)
        clear_basket()
        return redirect('/process/interview')


@process_app.route('/interview/clear')
def clear_basket_handler():
    clear_basket()
    return redirect('/process/interview/add')


@process_app.route('/interview/hire', methods=['GET', 'POST'])
def hire():
    if request.method == 'POST':
        i_id = request.form.get('i_id')
        result = request.form.get('result')
        if result == 'applied':
            sql = provider.get('candidate_by_interview.sql', i_id=i_id)
            item, _ = work_with_db(current_app.config['DB_CONFIG'], sql)
            name = item[0]['name']
            birthday = item[0]['birthday']
            education = item[0]['education']
            insert_employee = provider.get('insert_employee.sql', name=name, birthday=birthday, education=education)
            update_db(current_app.config['DB_CONFIG'], insert_employee)
            interview_result = provider.get('set_result_interview.sql', result=result, i_id=i_id)
            update_db(current_app.config['DB_CONFIG'], interview_result)
            return redirect('/process/interview')
        else:
            interview_result = provider.get('set_result_interview.sql', result=result, i_id=i_id)
            update_db(current_app.config['DB_CONFIG'], interview_result)
            return redirect('/process/interview')
