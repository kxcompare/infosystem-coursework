import json

from flask import Flask, render_template, session

import access

app = Flask(__name__)

dbconfig = json.load(open('configs/dbconfig.json'))
access_config = json.load(open('configs/access.json'))

app.config['SECRET_KEY'] = 'secret key'
app.config['DB_CONFIG'] = dbconfig
app.config['ACCESS_CONFIG'] = access_config

from bp_query.routes import query_app
from bp_auth.routes import auth_app
from bp_process.routes import process_app

app.register_blueprint(query_app, url_prefix='/query')
app.register_blueprint(auth_app, url_prefix='/auth')
app.register_blueprint(process_app, url_prefix='/process')


@app.route('/')
def index():
    context = {'group_permission_validation': access.group_permission_validation, 'role': access.get_role()}
    return render_template('main_menu.html', context=context)


@app.route('/exit')
def exit_system():
    session.clear()
    return render_template('exit.html')


if __name__ == '__main__':
    app.run(host='localhost', port=5001)
